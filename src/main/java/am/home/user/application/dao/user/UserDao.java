package am.home.user.application.dao.user;

import am.home.user.application.entity.User;

import java.util.List;

public interface UserDao extends GenericDao<User>{

    @Override
    User save(User user);

    @Override
    User update(Long id, User user);

    @Override
    void delete(Long id);

    @Override
    User find(Long id);

    @Override
    List<User> findAll();
}
