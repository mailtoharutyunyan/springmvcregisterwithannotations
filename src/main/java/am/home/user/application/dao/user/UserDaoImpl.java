package am.home.user.application.dao.user;

import am.home.user.application.entity.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserDaoImpl implements UserDao {


    @Autowired
    public SessionFactory myHibernateSessionFactory;


    @Override
    public User save(User user) {
        Transaction transaction = null;
        try (Session session = myHibernateSessionFactory.openSession()) {
            transaction = session.getTransaction();
            transaction.begin();

            session.persist(user);

            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return user;
    }


    @Override
    public User update(Long id, User user) {
        User user1 = find(id);
        if (user1.getId().equals(id)) {
            myHibernateSessionFactory.getCurrentSession().update(user);
            return user;
        }
        return null;

    }

    @Override
    public void delete(Long id) {
        User user = myHibernateSessionFactory.getCurrentSession().load(User.class, id);
        if (null != user) {
            myHibernateSessionFactory.openSession().delete(user);
        }
    }

    @Override
    public User find(Long id) {
        return myHibernateSessionFactory.openSession().get(User.class, id);
    }

//    @SuppressWarnings("unchecked")
    @Override
    public List<User> findAll() {
        Query query = myHibernateSessionFactory.openSession().createQuery("from User");
        List <User>list = query.list();
        myHibernateSessionFactory.close();
        if ((list == null) || (list.size() == 0)) {
            return list;
        }
        return list;
    }

}
