package am.home.user.application.dao.user;

import java.util.List;

public interface GenericDao<T> {

    T save(T t);

    T update(Long id, T t);

    void delete(Long id);

    T find(Long id);

    List<T> findAll();

}
