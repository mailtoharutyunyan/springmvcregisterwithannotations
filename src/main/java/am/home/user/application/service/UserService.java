package am.home.user.application.service;

import am.home.user.application.dto.request.UserRequestDto;
import am.home.user.application.dto.response.UserResponseDto;

import java.util.List;

public interface UserService {

    UserResponseDto save(UserRequestDto userRequestDto);

    UserResponseDto update(Long id, UserRequestDto userRequestDto);

    UserResponseDto delete(Long id);

    UserResponseDto find(Long id);

    List<UserResponseDto> findALl();

}
