package am.home.user.application.service;

import am.home.user.application.dao.user.UserDao;
import am.home.user.application.dto.request.UserRequestDto;
import am.home.user.application.dto.response.UserResponseDto;
import am.home.user.application.entity.User;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;

    @Override
    public UserResponseDto save(UserRequestDto userRequestDto) {
        User user = new User();
        BeanUtils.copyProperties(userRequestDto, user);
        userDao.save(user);
        UserResponseDto userResponseDto = new UserResponseDto();
        BeanUtils.copyProperties(userRequestDto, userResponseDto);
        return userResponseDto;
    }

    @Override
    public UserResponseDto update(Long id, UserRequestDto userRequestDto) {
        User user = userDao.find(id);
        BeanUtils.copyProperties(userRequestDto, user);
        if (user.getId().equals(id)) {
            userDao.update(id, user);
        }
        UserResponseDto userResponseDto = new UserResponseDto();
        BeanUtils.copyProperties(user, userResponseDto);
        return userResponseDto;
    }

    @Override
    public UserResponseDto delete(Long id) {
        User user = userDao.find(id);
        userDao.delete(id);
        UserResponseDto userResponseDto = new UserResponseDto();
        BeanUtils.copyProperties(user, userResponseDto);
        return userResponseDto;
    }

    @Override
    public UserResponseDto find(Long id) {
        UserResponseDto userResponseDto = new UserResponseDto();
        User user = userDao.find(id);
        BeanUtils.copyProperties(user, userResponseDto);
        return userResponseDto;
    }

    @Override
    @Transactional
    public List<UserResponseDto> findALl() {
        List<UserResponseDto> userResponseDtoArrayList = new ArrayList<>();
        List<User> userList = userDao.findAll();
        BeanUtils.copyProperties(userResponseDtoArrayList, userList);
        return userResponseDtoArrayList;
    }
}
